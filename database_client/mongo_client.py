# pip install pymongo
from datetime import datetime

import gridfs
from pymongo import MongoClient, ReturnDocument
from bson import ObjectId


class MongoCliStream(object):
    def __init__(self):
        self.client = MongoClient("mongodb://localhost:27017/")
        self.db = self.client['stream_db']
        self.collection = self.db['stream']
        self.fs = gridfs.GridFS(self.db)

    def insert_data(self, stream, stream_id, topic):
        if not self.find_document_by_id(stream_id):
            try:
                now = datetime.now()
                dt_string = now.strftime("%d/%m/%Y %H:%M")
                doc = self.collection.insert_one({'_id': ObjectId(stream_id), "StreamDate": dt_string, topic: stream})
            except Exception as e:
                print(f'\n[x] ERROR - INSERTED [x]: {e}')
            else:
                print('\n[!] INSERTED [!]')
                return doc.inserted_id

        elif self.find_document_by_id(stream_id) and topic == "StreamText":

            doc = self.find_document_by_id(stream_id)
            print(stream, doc)

            doc['StreamText']['Sentence'].append(stream['Sentence'][0])
            doc['StreamText']['Wpm'].append(stream['Wpm'][0])
            try:
                self.collection.find_one_and_update(
                    {'_id': ObjectId(stream_id)},
                    {'$set': {topic: doc['StreamText']}},
                    return_document=ReturnDocument.AFTER,
                )
            except Exception as e:
                print(f'\n[x] ERROR - ADD NEW SENTENCE VALUES [x]: {e}')
            else:
                print('\n[!] ADD NEW SENTENCE VALUES [!]')

        else:
            try:
                self.collection.find_one_and_update(
                    {'_id': ObjectId(stream_id)},
                    {'$set': {topic: stream}},
                    return_document=ReturnDocument.AFTER,
                )
            except Exception as e:
                print(f'\n[x] ERROR - ADD NEW VALUES [x]: {e}')
            else:
                print('\n[!] ADD NEW VALUES [!]')

    def insert_media_file(self, _id, file_location):
        name = file_location
        file_data = open(name, "rb")
        data = file_data.read()
        fs = gridfs.GridFS(self.db)
        fs.put(data, filename=name, _id=_id)
        print("Inserted Media File: ", _id)

    def download_media_file(self, _id):
        fs = gridfs.GridFS(self.db)
        name = str(_id) + ".wav"
        out_data = fs.get(ObjectId(_id)).read()
        out = open(name, "wb")
        out.write(out_data)
        out.close()

    def check_if_media_exists(self, _id):
        fs = gridfs.GridFS(self.db)
        out_data = fs.find_one(ObjectId(_id))
        if out_data:
            return True
        return False

    def find_documents(self, limit=5):
        return self.collection.find().limit(limit)

    def find_all_documents(self):
        docs = []
        documents = self.collection.find()
        for doc in documents:
            docs.append({'_id': str(doc['_id']), '_date': doc['StreamDate']})
        return docs

    def find_document_by_id(self, doc_id):
        return self.collection.find_one({'_id': ObjectId(doc_id)})

    def check_document_by_id(self, doc_id):
        if self.collection.find_one({'_id': ObjectId(doc_id)}):
            return True
        return False

    def remove_document(self, doc_id):
        try:
            self.collection.delete_one(
                {'_id': ObjectId(doc_id)}
            )
        except Exception as e:
            print('\n[x] ERROR [x]: {e}')
        else:
            print('\n[!] REMOVED [!]')

    def remove_all_documents(self):
        for x in self.find_documents():
            self.remove_document(x['_id'])


mc = MongoCliStream()
mc.remove_all_documents()
