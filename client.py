import math
import socket
import struct
import time
import pyaudio
from bson import ObjectId
#c
# Socket
PORT = 5000

# Audio
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
CHUNK = 1024

SWIDTH = 2
SHORT_NORMALIZE = (1.0 / 32768.0)


def rms(_frame):
    count = len(_frame) / SWIDTH
    f = "%dh" % count
    shorts = struct.unpack(f, _frame)

    sum_squares = 0.0
    for sample in shorts:
        n = sample * SHORT_NORMALIZE
        sum_squares += n * n
    _rms = math.pow(sum_squares / count, 0.5)
    return _rms * 1000


def generate_unique_id():
    return ObjectId()


p = pyaudio.PyAudio()
ps_stream = p.open(format=FORMAT,
                   channels=CHANNELS,
                   rate=RATE,
                   input=True,
                   frames_per_buffer=CHUNK)


def recording():
    current = time.time()
    end = time.time() + 2

    while current <= end:
        data = ps_stream.read(1600)
        if rms(data) < 10:
            pass
        else:
            end = time.time() + 2
        current = time.time()
        client_socket.send(data)


with socket.socket() as client_socket:
    client_socket.connect((HOST, PORT))
    while True:
        frame = ps_stream.read(1600)
        if rms(frame) > 10:
            new_id = generate_unique_id()

            print('Noise Detected:', new_id)
            client_socket.send(b'Recording')
            recording()
        else:
            client_socket.send(b"Silence")
