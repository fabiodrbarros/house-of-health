import requests

from bot.text_to_speech import text_to_speech

machine_ip = 'localhost'
sender = "User"


def talk_to_bot(message):

    print("Sending message now: " , message )

    r = requests.post('http://' + machine_ip + ':5002/webhooks/rest/webhook',
                      json={"sender": sender, "message": message})

    for i in r.json():
        try:
            bot_message = i['text']
            print(bot_message)
            text_to_speech(text=bot_message)
        except:
            print("Image")


