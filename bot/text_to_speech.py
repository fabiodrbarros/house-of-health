from azure.cognitiveservices.speech import SpeechConfig, SpeechSynthesizer
from azure.cognitiveservices.speech.audio import AudioOutputConfig
from pydub import AudioSegment
from pydub.playback import play

credential = "02e1f55b170a4866b332066783a58ca8"
endpoint = "southcentralus"
audio_file = "file.wav"


def text_to_speech(language="pt-PT", text="Text"):
    speech_config = SpeechConfig(subscription=credential, region=endpoint)

    speech_config.speech_synthesis_language = language  # e.g. "de-DE"
    speech_config.speech_synthesis_voice_name = "pt-PT-DuarteNeural"

    audio_config = AudioOutputConfig(filename=audio_file)
    synthesizer = SpeechSynthesizer(speech_config=speech_config, audio_config=audio_config)
    text = "... " + text
    synthesizer.speak_text_async(text)
    song = AudioSegment.from_wav(audio_file)
    play(song)
