from kafka import KafkaProducer
import json


class Producer:
    def __init__(self):
        self.ip = "localhost"
        # self.bootstrap_servers = ["kafka1:19091"]
        self.bootstrap_servers = [self.ip + ':9092']
        self.producer = self.init_producer()

    def init_producer(self):
        producer = KafkaProducer(bootstrap_servers=self.bootstrap_servers,
                                 value_serializer=lambda v: json.dumps(v).encode('utf-8'))
        return producer

    def send_message(self, message, topic):
        self.producer.send(topic, message)
        self.producer.flush()



"""x = {"Values": [0]}

p = Producer()
p.send_message(x, 'text-stream')

p.send_message(x, 'sentiment-analysis-stream')
#p.send_message(x, "sentiment-analysis-steam")"""

