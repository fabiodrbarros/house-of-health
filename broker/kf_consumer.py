import sys
import json
import numpy as np
from kafka import KafkaConsumer

sys.path.append('../')

from bot.bot_endpoint import talk_to_bot
from database_client.mongo_client import MongoCliStream


class Consumer:
    def __init__(self):
        self.ip = "localhost"
        self.bootstrap_servers = [self.ip + ':9092']
        self.topics = ['stream-audio', 'stream-audio-features', 'stream-sentiment', 'stream-text', 'head-orientation',
                       'proxemics']
        self.consumer = self.init_consumer()
        self.mc = MongoCliStream()

    def init_consumer(self):
        consumer = KafkaConsumer(bootstrap_servers=self.bootstrap_servers,
                                 value_deserializer=lambda m: json.loads(m))
        consumer.subscribe(topics=self.topics)
        return consumer

    def get_message(self):

        for message in self.consumer:
            data = message.value
            stream_id = data["Stream_id"]
            if message.topic == 'stream-text':
                print(data)
                self.mc.insert_data(data, stream_id, 'StreamText')
            if message.topic == 'stream-audio-features':
                if not np.isnan(data['Energy']).any():
                    self.mc.insert_data(data, stream_id, 'StreamAudioFeatures')
            if message.topic == 'stream-sentiment':
                self.mc.insert_data(data, stream_id, 'StreamSentiment')


c1 = Consumer()
c1.get_message()
