import socket
import struct
import threading
import wave
import os
import pyaudio
import math

from bson import ObjectId
from database_client.mongo_client import MongoCliStream

from speech.open_smile import OpenSmile
from speech.speech_to_text import ContinuousSpeechStream


def generate_unique_id():
    return ObjectId()


# Socket
HOST = socket.gethostname()
PORT = 5000

# Audio
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
CHUNK = 1024

SWIDTH = 2
SHORT_NORMALIZE = (1.0 / 32768.0)



def rms(frame):
    count = len(frame) / SWIDTH
    f = "%dh" % count
    shorts = struct.unpack(f, frame)

    sum_squares = 0.0
    for sample in shorts:
        n = sample * SHORT_NORMALIZE
        sum_squares += n * n
    rms = math.pow(sum_squares / count, 0.5)
    return rms * 1000


def get_data_from_client(conn, address):
    stream_id = generate_unique_id()
    frames = []
    data = conn.recv(1600)
    recording = False

    while data != "":
        try:

            data = conn.recv(1600)

            if data != b'Silence' and data != b'Recording':
                css.set_stream_id(str(stream_id))
                css.stream.write(data)
                recording = True
                frames.append(data)

            if data == b'Recording':
                stream_id = generate_unique_id()
                print('Noise Detected:', stream_id)

            if data == b'Silence' and recording == True and frames != []:
                if not mc.check_if_media_exists(str(stream_id)):
                    with wave.open('temp_files/' + str(stream_id) + ".wav", 'wb') as wf:
                        wf.setnchannels(CHANNELS)
                        wf.setsampwidth(p.get_sample_size(FORMAT))
                        wf.setframerate(RATE)
                        wf.writeframes(b''.join(frames))
                    print("Saved file: ", 'temp_files/' + str(stream_id) + ".wav")
                    mc.insert_media_file(stream_id, 'temp_files/' + str(stream_id) + ".wav")
                    op.extract_features_from_file('temp_files/' + str(stream_id) + ".wav", stream_id)
                    os.remove('temp_files/' + str(stream_id) + ".wav")

                frames = []
                recording = False

        except socket.error:
            print("Client Disconnected")
            conn.close()
            break


p = pyaudio.PyAudio()
ps_stream = p.open(format=FORMAT,
                   channels=CHANNELS,
                   rate=RATE,
                   output=True,
                   input=True,
                   frames_per_buffer=CHUNK)
print(p.get_sample_size(FORMAT))

op = OpenSmile()
css = ContinuousSpeechStream()
mc = MongoCliStream()

thread = threading.Thread(target=css.speech_recognition_with_push_stream)
thread.start()

with socket.socket() as server_socket:
    server_socket.bind((HOST, PORT))
    server_socket.listen(500)

    while True:
        con, add = server_socket.accept()
        print("Connection from " + add[0] + ":" + str(add[1]))
        threading.Thread(target=get_data_from_client, args=(con, add)).start()


# server_socket.close()
