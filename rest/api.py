import os
import sys
from flask import Flask, jsonify, request, send_file
from flask_cors import CORS, cross_origin
from statistics import mean

sys.path.append('../')

from database_client.mongo_client import MongoCliStream



app = Flask(__name__)
CORS(app)
mongo = MongoCliStream()


@app.route('/getStreams')
def get_streams():
    return jsonify(mongo.find_all_documents())


@app.route('/getStreamInfo/<string:stream_id>', methods=["GET"])
def get_stream_info(stream_id):
    stream_info = mongo.find_document_by_id(stream_id)
    wpm_temp = stream_info['StreamText']['Wpm']
    mean_value = mean(wpm_temp)
    print(mean_value)
    stream_info['StreamText']['Wpm'] = mean_value
    stream_info.update({'_id': stream_id})
    return jsonify(stream_info)


"""@app.route('/get_user_moods/<string:user_id>', methods=["GET"])
def get_user_moods(user_id):
    user_info = mongo_user.find_document_by_id(user_id)
    moods = user_info['UserInfo']['Moods']
    moods.sort(key=lambda x: datetime.strptime(x['Date'], '%d/%m/%Y %H:%M'))
    moods = list(reversed(moods))
    # stream_info.update({'_id': stream_id})
    return jsonify(moods)"""


@app.route('/getStreamMedia/<string:stream_id>')
@cross_origin()
def getStreamMedia(stream_id):
    mongo.download_media_file(stream_id)
    stem_path = f"{stream_id}.wav"
    if os.path.isfile(stem_path):
        return send_file(stem_path, mimetype="audio/wav", as_attachment=True)
    else:
        return {"message": "cannot download stem"}, 401


app.run(debug=True, host="localhost", port=4998)
