import opensmile
import numpy as np

from broker.kf_producer import Producer


class OpenSmile(object):
    def __init__(self):
        self.smile = opensmile.Smile(
            feature_set=opensmile.FeatureSet.ComParE_2016,
            feature_level=opensmile.FeatureLevel.LowLevelDescriptors,
        )
        self.smile_2 = opensmile.Smile(
            feature_set=opensmile.FeatureSet.emobase,
            feature_level=opensmile.FeatureLevel.LowLevelDescriptors,
        )
        self.producer = Producer()
        self.topic = 'stream-audio-features'

    def features_available(self):
        return self.smile.feature_names

    def extract_features_from_file(self, file_name, stream_id):
        y = self.smile.process_file(file_name)
        data = {"Stream_id": str(stream_id), "Energy": y['pcm_RMSenergy_sma'].ravel().tolist(),
                'F0': y['F0final_sma'].ravel().tolist()}

        y = self.smile_2.process_file(file_name)
        data.update({"Intensity": y['pcm_intensity_sma'].ravel().tolist()})

        self.producer.send_message(data, self.topic)

        return data

    def extract_features_from_stream(self, in_data, rate):
        numpy_array = np.frombuffer(in_data, dtype=np.float32)
        y = self.smile.process_signal(numpy_array, rate)
        print(y)
