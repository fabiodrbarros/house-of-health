#!/usr/bin/env python

import json
import threading
from broker.kf_producer import Producer
from speech.sentiment_analysis_azure import SentimentAnalysis

try:
    import azure.cognitiveservices.speech as speechsdk
except ImportError:
    print("""
    Importing the Speech SDK for Python failed.
    Refer to
    https://docs.microsoft.com/azure/cognitive-services/speech-service/quickstart-python for
    installation instructions.
    """)
    import sys

    sys.exit(1)


def hns_to_seconds(nano_seconds_start, nano_seconds_end):
    seconds_start = (nano_seconds_start / 10000) / 1000.0
    seconds_end = seconds_start + (nano_seconds_end / 10000) / 1000.0
    return round(seconds_start, 2), round(seconds_end, 2)


def wpm(sentence, start, duration):
    sentence = ' '.join(sentence)
    d = hns_to_seconds(start, duration)
    number_of_words = len(sentence.split(' '))
    t = d[1] - d[0]
    return round((60 * number_of_words) / t, 2)


class ContinuousSpeechStream:
    def __init__(self, language='pt-PT', sentiment_analysis=True):
        self.language = language
        self.key = '02e1f55b170a4866b332066783a58ca8'
        self.end_point = 'southcentralus'
        self.stream = speechsdk.audio.PushAudioInputStream()
        self.client = self.auth_client()
        self.topic = 'stream-text'
        self.p = Producer()
        self.stream_id = 0
        if sentiment_analysis:
            self.sa = SentimentAnalysis()

    def set_stream_id(self, new_id):
        self.stream_id = new_id

    def auth_client(self):
        audio_config = speechsdk.audio.AudioConfig(stream=self.stream)

        speech_config = speechsdk.SpeechConfig(subscription=self.key, region=self.end_point)
        speech_config.speech_recognition_language = self.language
        speech_config.request_word_level_timestamps()
        speech_config.request_word_level_timestamps()

        return speechsdk.SpeechRecognizer(speech_config=speech_config, audio_config=audio_config)

    def speech_recognition_with_push_stream(self):
        recognition_done = threading.Event()

        def parse_azure_result(evt):
            transcript_display_list = []
            words = []
            response = json.loads(evt.result.json)
            transcript_display_list.append(response['DisplayText'])
            confidence_list_temp = [item.get('Confidence') for item in response['NBest']]
            max_confidence_index = confidence_list_temp.index(max(confidence_list_temp))

            words.extend(response['NBest'][max_confidence_index]['Words'])

            sentence = {"Stream_id": self.stream_id, "Sentence": transcript_display_list}
            segmentation = []

            for word in words:
                start, end = hns_to_seconds(word['Offset'], word['Duration'])
                segmentation.append({"Word": word["Word"], "Start": start, "End": end})
            sentence.update({"Wpm": [wpm(transcript_display_list, response['Offset'], response['Duration'])]})
            # sentence.update({"WordsSegmentation": segmentation})
            self.p.send_message(sentence, self.topic)

            self.sa.sentiment_analysis(sentence['Sentence'], self.stream_id)

        def stop_cb(evt):
            print('SESSION STOPPED {}'.format(evt))
            recognition_done.set()

        # self.client.recognizing.connect(lambda evt: print('RECOGNIZING: {}'.format(evt)))
        # self.client.recognized.connect(lambda evt: print('RECOGNIZED: {}'.format(evt)))
        self.client.recognized.connect(lambda evt: parse_azure_result(evt))
        self.client.session_started.connect(lambda evt: print('SESSION STARTED: {}'.format(evt)))
        self.client.session_stopped.connect(stop_cb)
        self.client.canceled.connect(lambda evt: print('CANCELED {}'.format(evt)))

        self.client.start_continuous_recognition()
        recognition_done.wait()
        self.client.stop_continuous_recognition()
