from azure.core.credentials import AzureKeyCredential
from azure.ai.textanalytics import TextAnalyticsClient
from broker.kf_producer import Producer


class SentimentAnalysis:
    def __init__(self):
        self.key = '02e1f55b170a4866b332066783a58ca8'
        self.end_point = "https://" + "southcentralus" + ".Api.cognitive.microsoft.com/"
        self.client = self.authenticate_client()
        self.producer = Producer()
        self.topic = "stream-sentiment"

    def authenticate_client(self):
        ta_credential = AzureKeyCredential(self.key)
        text_analytics_client = TextAnalyticsClient(
            endpoint=self.end_point,
            credential=ta_credential)

        return text_analytics_client

    # documents, show_opinion_mining = True
    def sentiment_analysis(self, sentence, stream_id):
        response = self.client.analyze_sentiment(documents=sentence)[0]
        sentiment = "Sentiment: {}".format(response.sentiment)
        sentiment += " ==> " + "Scores: positive={0:.2f}; neutral={1:.2f}; negative={2:.2f}".format(
            response.confidence_scores.positive,
            response.confidence_scores.neutral,
            response.confidence_scores.negative,
        )
        data = {"Stream_id": stream_id, "Sentiment": response.sentiment, "Scores": [response.confidence_scores.positive,
                                                                                    response.confidence_scores.neutral,
                                                                                    response.confidence_scores.negative]}

        self.producer.send_message(data, self.topic)
        return sentiment


# Test

"""sa = SentimentAnalysis()
x = sa.sentiment_analysis(["I had the best day of my life. But also sad..."])
print(x)"""
