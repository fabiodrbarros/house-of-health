import asyncio
import audioop
import os
import ssl
import threading
import wave

import websockets
from bson import ObjectId

from database_client.mongo_client import MongoCliStream
from speech.open_smile import OpenSmile
from speech.speech_to_text_v2 import ContinuousSpeechFile


def generate_unique_id():
    return ObjectId()


async def get_audio_stream(websocket):
    stream_id = generate_unique_id()
    frames = []

    async for message in websocket:
        if message == 'start':
            stream_id = generate_unique_id()
            print('New Stream ', str(stream_id))

        elif message == 'stop':
            print("Recording Stopped")

        elif message == 'analyze':
            print('Processing')
            if not mc.check_if_media_exists(str(stream_id)):
                with wave.open('temp_files/' + str(stream_id) + ".wav", 'wb') as wf:
                    wf.setnchannels(1)
                    wf.setsampwidth(2)
                    wf.setframerate(48000)
                    wf.writeframes(b''.join(frames))
                print("Saved file: ", 'temp_files/' + str(stream_id) + ".wav")
                css.recognize_continuous_speech('temp_files/' + str(stream_id) + ".wav", str(stream_id))
                mc.insert_media_file(stream_id, 'temp_files/' + str(stream_id) + ".wav")
                op.extract_features_from_file('temp_files/' + str(stream_id) + ".wav", stream_id)
                os.remove('temp_files/' + str(stream_id) + ".wav")
                frames = []

        else:
            print("Recording")
            frames.append(message)


op = OpenSmile()
css = ContinuousSpeechFile()
mc = MongoCliStream()

ssl_cert = "cert.pem"
ssl_key = "key.pem"

ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
ssl_context.load_cert_chain(ssl_cert, keyfile=ssl_key)


async def main():
    async with websockets.serve(get_audio_stream, '192.168.0.200', 4999, ssl=ssl_context):
        await asyncio.Future()


asyncio.run(main())
